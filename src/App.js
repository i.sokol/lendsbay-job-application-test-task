import React, { useState, useCallback } from 'react'
import { useTranslation } from "react-i18next"

import './App.css'
import Table from './components/Table/Table'
import DataEditor from "./components/DataEditor/DataEditor"

const tempData = [{ name: 'Test1', value: 'Test2' }, { name: 'Test3', value: 'Test4' }]

const App = () => {
    const [data, setData] = useState(tempData)
    const [lang, setLang] = useState('en')
    const { t, i18n } = useTranslation()

    const changeLanguage = useCallback((l) => {
        setLang(l)
        i18n.changeLanguage(l)
    }, [i18n])

    return <div className="App">
        <div className="row justify-content-end align-items-center btn-group-sm">
            <span>{t('langs.title')}</span>
            <button className={`btn btn-secondary ml-2 ${lang === 'ru' && "active"}`} disabled={lang === 'ru'} onClick={() => changeLanguage('ru')}>{t('langs.ru')}</button>
            <button className={`btn btn-dark ml-1 mr-5 ${lang === 'en' && "active"}`} disabled={lang === 'en'} onClick={() => changeLanguage('en')}>{t('langs.en')}</button>
        </div>
        <Table data={data} setData={setData} />
        <DataEditor json={data} save={setData} />
    </div>
}

export default App
