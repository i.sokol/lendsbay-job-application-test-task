import React, { memo, useCallback, useState, useEffect, useRef } from 'react'
import { useTranslation } from 'react-i18next'

import './DataEditor.css'

const DataEditor = memo(({ json, save }) => {
    const { t } = useTranslation()

    const [data, setData] = useState(JSON.stringify(json, null, 2))
    const [error, setError] = useState(false)
    const [autoSave, setAutoSave] = useState(true)

    const textAreaRef = useRef(null)

    useEffect(() => {
        setData(JSON.stringify(json, null, 2))
    }, [json])

    useEffect(() => {
        if (textAreaRef) {
            textAreaRef.current.setCustomValidity(error ? "Non  valid JSON entered" : "")
        }
    }, [error])

    const validateJSON = useCallback(() => {
        try {
            return JSON.parse(data)
        } catch (e) {
            return false
        }
    }, [data])

    const saveData = useCallback(() => {
        const jsonData = validateJSON()
        if (!jsonData) {
            setError(true)
            return
        }
        setError(false)
        save(jsonData)
    }, [save, validateJSON])

    const onChange = useCallback((event) => {
        const newData = event.target.value
        setData(newData)
    }, [])

    const onFocus = useCallback(() => {
        setError(false)
    }, [])

    const onBlur = useCallback(() => {
        if (autoSave) {
            saveData()
        } else {
            setError(!validateJSON())
        }
    }, [autoSave, saveData, validateJSON])

    const onCheckChange = useCallback(() => {
        setAutoSave(!autoSave)
    }, [autoSave])

    return (
        <div className="data-editor">
            <h3>{t('editor.title')}</h3>
            <p className={`error ${error && "error-visible"}`}>{t('errors.json')}</p>
            <textarea
                ref={textAreaRef}
                value={data}
                {...{
                    onChange,
                    onFocus,
                    onBlur,
                }}
            />
            <div align="justify" className="row btn-group-lg">
                <label className="ml-3 mr-sm-5 mr-3 px-auto px-sm-0 mb-1 mb-sm-0 col-sm-4 col-md-3 btn btn-secondary">
                    <input className="mr-2" type="checkbox" checked={autoSave} onChange={onCheckChange} />
                    {t('editor.autosave')}
                </label>
                <button className="ml-sm-5 pl-sm-5 ml-3 mr-3 col btn btn-primary" onChange={saveData} disabled={autoSave}>{t('editor.save')}</button>
            </div>
        </div>
    )
})

export default DataEditor
