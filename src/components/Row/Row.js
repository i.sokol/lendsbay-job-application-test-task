import React, { memo, useState, useCallback, useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import { keys } from "../../config"

const Row = memo(({ index, rowData, updateRow, deleteRow, firstItem, lastItem, swapRows, isEditing = false, onEditingCancel }) => {
    const { t } = useTranslation()

    const [editing, setEditing] = useState(isEditing)
    const [editedData, setEditedData] = useState(Object.assign({}, rowData))

    useEffect(() => {
        setEditedData(Object.assign({}, rowData))
    }, [rowData])

    const onRowEdit = useCallback(() => {
        setEditing(true)
    }, [])

    const onRowSave = useCallback(() => {
        updateRow(index, editedData)
        setEditing(false)
    }, [index, editedData, updateRow])

    const onInputChange = useCallback((event, key) => {
        const updatedEditedData = Object.assign({}, editedData)
        updatedEditedData[key] = event.target.value
        setEditedData(updatedEditedData)
    }, [editedData])

    const onEditingCancelPressed = useCallback(() => {
        if (onEditingCancel) {
            onEditingCancel()
        }
        setEditing(false)
        setEditedData(Object.assign({}, rowData))
    }, [rowData, onEditingCancel])

    return <tr>
        <th scope="row">{index + 1}</th>
        {keys.map((key, index) => <td key={`cell-${index}`}>{ editing ?
            <input type="text" className="form-control form-control-sm editing-form" value={editedData[key]} onChange={(event) => onInputChange(event, key)} />
            : rowData[key] || ''
        }</td>)}
        <td className="btn-group-sm row-cols-sm-4">
            {editing ?
                <>
                    <button className="col-sm-5 btn btn-outline-primary" onClick={onRowSave}>{t('table.rowControls.save')}</button>
                    <button className="mt-2 mt-sm-0 col-sm-5 ml-sm-2 btn btn-outline-secondary" onClick={onEditingCancelPressed}>{t('table.rowControls.cancel')}</button>
                </>
                : <>
                    <button disabled={firstItem} className="col-sm-1 px-0 btn btn-outline-secondary" onClick={() => swapRows(index, index - 1)}>&#11014;</button>
                    <button disabled={lastItem} className="mt-2 mt-sm-0 col-sm-1 ml-sm-2 px-0 btn btn-outline-secondary" onClick={() => swapRows(index, index + 1)}>&#11015;</button>
                    <button className="mt-2 mt-sm-0 col-sm-4 ml-sm-2 btn btn-outline-primary" onClick={onRowEdit}>{t('table.rowControls.edit')}</button>
                    <button className="mt-2 mt-sm-0 col-sm-4 ml-sm-2 btn btn-outline-danger" onClick={() => deleteRow(index)}>{t('table.rowControls.delete')}</button>
                </>
            }
        </td>
    </tr>
})

Row.displayName = 'Row'

export default Row
