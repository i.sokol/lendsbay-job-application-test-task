import React, { memo, useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'

import Row from "../Row/Row"
import { keys } from "../../config"

import './Table.css'

const Table = memo(({ data, setData }) => {
    const { t, i18n } = useTranslation()

    const [addNew, setAddNew] = useState(false)

    const updateRow = useCallback((index, updatedRow) => {
        const updatedData = [...data]
        updatedData[index] = updatedRow
        setData(updatedData)
    }, [data, setData])

    const deleteRow = useCallback((index) => {
        const conf = window.confirm('Are you sure you want to delete row?')
        if (conf) {
            const updatedData = [...data]
            updatedData.splice(index, 1)
            setData(updatedData)
        }
    }, [data, setData])

    const clearData = useCallback(() => {
        const conf = window.confirm('Are you sure you want to clear data?')
        if (conf) {
            setData([])
        }
    }, [setData])

    const swapRows = useCallback((first, second) => {
        const updatedData = [...data]
        const temp = updatedData[first]
        updatedData[first] = updatedData[second]
        updatedData[second] = temp
        setData(updatedData)
    }, [data, setData])

    const onAddNewPressed = useCallback(() => {
        setAddNew(true)
    }, [])

    const onAddNewCanceled = useCallback(() => {
        setAddNew(false)
    }, [])

    const addNewRow = useCallback((_, newRow) => {
        const updatedData = [...data]
        updatedData.push(newRow)
        setData(updatedData)
        setAddNew(false)
    }, [data, setData])

    return <div className="data-table">
        <h3>{t('table.title')}</h3>
        <div className="table-responsive">
            <table className="table table-striped table-bordered table-hover">
                <thead className="thead-light">
                    <tr>
                        <th scope="row" className="num-row">#</th>
                        {keys.map((heading, index) =>
                            <th key={`heading-${index}`} scope="col">
                                { heading.charAt(0).toUpperCase() + heading.slice(1) }
                            </th>
                        )}
                        <th className={`btn-group-sm row-cols-sm-2 ${i18n.language === 'en' ? "controls" : "controls-ru"}`}>
                            <button disabled={addNew} className="col-sm-5 btn btn-primary" onClick={onAddNewPressed}>{t('table.controls.add')}</button>
                            <button className="mt-2 mt-sm-0 col-sm-5 ml-sm-2 btn btn-danger" onClick={clearData}>{t('table.controls.clear')}</button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((rowData, index) =>
                        <Row key={`row-${index}`} {...{ rowData, index, updateRow, deleteRow, firstItem: index === 0, lastItem: index === data.length - 1, swapRows }} />
                    )}
                </tbody>
                <tfoot>
                    {addNew && <Row rowData={({name: '', value: ''})} index={data.length} updateRow={addNewRow} deleteRow={() => {}} firstItem={false} lastItem={true} swapRows={() => {}} isEditing={true} onEditingCancel={onAddNewCanceled} />}
                </tfoot>
            </table>
        </div>
    </div>
})

Table.displayName = 'Table'

export default Table
