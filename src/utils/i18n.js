import i18n from "i18next"
import { initReactI18next } from "react-i18next"

const resources = {
    en: {
        translation: {
            errors: {
                json: "Non valid JSON entered",
            },
            editor: {
                save: "Save",
                autosave: "Autosave",
                title: "JSON Data editor",
            },
            table: {
                title: "Table Data editor",
                rowControls: {
                    save: "Save",
                    cancel: "Cancel",
                    edit: "Edit",
                    delete: "Delete",
                },
                controls: {
                    add: "Add",
                    clear: "Clear",
                },
            },
            langs: {
                title: 'Choose language',
                en: "English",
                ru: "Russian",
            },
        },
    },
    ru: {
        translation: {
            errors: {
                json: "Введён невалидный JSON",
            },
            editor: {
                save: "Сохранить",
                autosave: "Автосохранение",
                title: "Редактор данных JSON",
            },
            table: {
                title: "Табличный редактор данных",
                rowControls: {
                    save: "Сохранить",
                    cancel: "Отменить",
                    edit: "Редактировать",
                    delete: "Удалить",
                },
                controls: {
                    add: "Добавить",
                    clear: "Очистить",
                },
            },
            langs: {
                title: 'Выбор языка',
                en: "Английский",
                ru: "Русский",
            },
        },
    },
}

i18n
    .use(initReactI18next)
    .init({
        resources,
        lng: "en",
        keySeparator: ".",
        interpolation: {
            escapeValue: false,
        },
    })

export default i18n
